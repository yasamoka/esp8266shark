import sys
import time
from esp8266 import *


PORT = "COM4"

esp8266 = ESP8266ATCommandMode(port=PORT)

# check if online
try:
  assert esp8266.attention()
except AssertionError:
  print("ESP8266 module offline, exiting...")
  sys.exit()

# reset configuration
output = esp8266.reset()
print(output)
print()

# get firmware information
firmware = esp8266.getFirmwareVersion()
print(firmware)
print()

# get Wi-Fi mode
mode = esp8266.getWiFiMode()
if mode == ESP8266ATCommandMode.WIFI_MODE_STA:
  print("ESP8266 module is in STA mode")
elif mode == ESP8266ATCommandMode.WIFI_MODE_AP:
  print("ESP8266 module is in AP mode")
elif mode == ESP8266ATCommandMode.WIFI_MODE_STA_AP:
  print("ESP8266 module is in STA+AP mode")
print()

# query Wi-Fi mode scope (get Wi-Fi mode range)
modeFrom, modeTo = esp8266.queryWiFiModeScope()
print("Wi-Fi mode scope: %i - %i" % (modeFrom, modeTo))
print()

# set Wi-Fi mode as current
esp8266.setWiFiMode(ESP8266ATCommandMode.WIFI_MODE_STA_AP)

# set Wi-Fi mode as default
esp8266.setWiFiMode(ESP8266ATCommandMode.WIFI_MODE_STA_AP, default=True)

# set Wi-Fi mode without mode validation
esp8266.setWiFiMode(3)

# join Wi-Fi AP
ssid = "testSSID"
pwd = "testPass12345"
errorCode = esp8266.joinWiFiAP(ssid=ssid, pwd=pwd)
if errorCode == ESP8266ATCommandMode.WIFI_AP_JOIN_CONNECTED:
  print('ESP8266 module connected to "%s"' % ssid)
elif errorCode == ESP8266ATCommandMode.WIFI_AP_JOIN_FAILED_TIMEOUT:
  print('ESP8266 module failed to connect to "%s" - timeout' % ssid)
elif errorCode == ESP8266ATCommandMode.WIFI_AP_JOIN_FAILED_WRONG_PASS:
  print('ESP8266 module failed to connect to "%s" - wrong password' % ssid)
elif errorCode == ESP8266ATCommandMode.WIFI_AP_JOIN_FAILED_SSID_NOT_FOUND:
  print('ESP8266 module failed to connect to "%s" - not found' % ssid)
elif errorCode == ESP8266ATCommandMode.WIFI_AP_JOIN_FAILED_OTHER:
  print('ESP8266 module failed to connect to "%s"' % ssid)

# get connected Wi-Fi AP
AP = esp8266.getConnectedWiFiAP()
if AP == -1:
  print("Not connected to any AP")
else:
  print(AP)
print()

# set Wi-Fi AP scan configuration
parameters = ESP8266ATCommandMode.WIFI_AP_SCAN_SHOW_SSID | \
             ESP8266ATCommandMode.WIFI_AP_SCAN_SHOW_SECURITY | \
             ESP8266ATCommandMode.WIFI_AP_SCAN_SHOW_RSSI | \
             ESP8266ATCommandMode.WIFI_AP_SCAN_SHOW_BSSID | \
             ESP8266ATCommandMode.WIFI_AP_SCAN_SHOW_CHANNEL | \
             ESP8266ATCommandMode.WIFI_AP_SCAN_SHOW_FREQ_OFFSET | \
             ESP8266ATCommandMode.WIFI_AP_SCAN_SHOW_FREQ_CAL
esp8266.setScanWiFiAPConfig(orderByRSSI=True, showParameters=parameters)

# scan for Wi-Fi APs in range
APs = esp8266.scanWiFiAPs()
APs = esp8266.scanWiFiAPs(ssid="mySSID")
APs = esp8266.scanWiFiAPs(ssid="mySSID", bssid="00:11:22:33:44:55") #has issues when used after previous scan
APs = esp8266.scanWiFiAPs(ssid="mySSID", bssid="00:11:22:33:44:55", channel=6) #has issues
for AP in APs:
  print(AP)
  print()
sys.exit()
# disconnect from Wi-Fi AP
esp8266.disconnectWiFiAP()

# get softAP mode configuration
AP = esp8266.getSoftAPConfig()
print(AP)
print()

# set softAP mode configuration
ssid = "testSSID"
pwd = "testPwd12345"
esp8266.setSoftAPConfig(ssid=ssid,
                        pwd=pwd,
                        channel=5,
                        security=ESP8266ATCommandMode.WIFI_SECURITY_WPA_WPA2_PSK)
esp8266.setSoftAPConfig(ssid=ssid,
                        pwd=pwd,
                        channel=5,
                        security=ESP8266ATCommandMode.WIFI_SECURITY_WPA_WPA2_PSK,
                        maxConn=2)
esp8266.setSoftAPConfig(ssid=ssid,
                        pwd=pwd,
                        channel=5,
                        security=ESP8266ATCommandMode.WIFI_SECURITY_WPA_WPA2_PSK,
                        maxConn=2,
                        ssidHidden=False)

# get connected stations (only in softAP mode)
stations = esp8266.getConnectedStations()
for IPAddr, MACAddr in stations:
  print('IP Address:\t%s\r\nMAC Address:\t%s' % (IPAddr, MACAddr))
  print()

# get DHCP state for softAP mode and station mode
softAPDHCP, stationDHCP = esp8266.getDHCPState()
print('Soft AP DHCP:\t%s\r\nStation DHCP:\t%s' % (softAPDHCP, stationDHCP))
print()

# set DHCP state for softAP mode, station mode, or both
esp8266.setDHCPState(mode=ESP8266ATCommandMode.DHCP_AP, state=True)
esp8266.setDHCPState(mode=ESP8266ATCommandMode.DHCP_AP, state=False)
esp8266.setDHCPState(mode=ESP8266ATCommandMode.DHCP_STA, state=True)
esp8266.setDHCPState(mode=ESP8266ATCommandMode.DHCP_STA_AP, state=True)

import serial
import time
import re
import sys
from datetime import datetime


class ESP8266ATCommandError(Exception):
  pass

class ESP8266TimeoutError(Exception):
  pass

class ESP8266Firmware:

  AT_VERSION_REGEX_PATTERN = "AT version:(\w+(.\w+)*)\((.+?)\)"
  SDK_VERSION_REGEX_PATTERN = "SDK version:(\w+(.\w+)*)\((.+?)\)"
  DATE_FORMAT = "%b %d %Y %H:%M:%S"

  def __init__(self, GMROutput):
    lines = GMROutput.split('\r\n')
    ATVersionMatch = re.match(self.AT_VERSION_REGEX_PATTERN, lines[0])
    SDKVersionMatch = re.match(self.SDK_VERSION_REGEX_PATTERN, lines[1])
    self.ATVersion = ATVersionMatch.group(1)
    ATVersionBuildDateStr = ATVersionMatch.group(3)
    self.ATVersionBuildDate = datetime.strptime(ATVersionBuildDateStr, self.DATE_FORMAT)
    self.SDKVersion = SDKVersionMatch.group(1)
    self.SDKVersionBuildName = SDKVersionMatch.group(3)
    self.vendorName = lines[2]
    self.time = datetime.strptime(lines[3], self.DATE_FORMAT)

  def getATVersion(self):
    return self.ATVersion

  def getATVersionBuildDate(self):
    return self.ATVersionBuildDate

  def getSDKVersion(self):
    return self.SDKVersion

  def getSDKVersionBuildName(self):
    return self.SDKVersionBuildName

  def getVendorName(self):
    return self.vendorName

  def getTime(self):
    return self.time.strftime(self.DATE_FORMAT)

  def __str__(self):
    return "AT Version: %s (%s)\r\nSDK Version: %s (%s)\r\nVendor: %s\r\nTime: %s" % (self. ATVersion, self.getATVersionBuildDate(), self.SDKVersion, self.SDKVersionBuildName, self.vendorName, self.getTime())

class ESP8266AccessPoint():
  def __init__(self):
    self.ssid = None
    self.security = None
    self.pwd = None
    self.rssi = None
    self.bssid = None
    self.channel = None
    self.freqOffset = None
    self.freqCal = None
    self.maxConn = None
    self.hidden = None

  def _setup1(self, CWJAPLine, default):
    pattern = '\+CWJAP_'
    if default:
      pattern += ESP8266ATCommandMode.DEF
    else:
      pattern += ESP8266ATCommandMode.CUR
    pattern += ':\"(.+?)\",\"(([0-9a-f]{2})(:([0-9a-f]{2})){5})\",(\d+),(-*\d+)'
    match = re.match(pattern, CWJAPLine)
    self.ssid = match.group(1)
    self.bssid = match.group(2)
    channelStr = match.group(6)
    self.channel = int(channelStr)
    rssiStr = match.group(7)
    self.rssi = int(rssiStr)

  def _setup2(self, CWLAPLine):
    pattern = "\+CWLAP:\((\d+),\"(.+?)\",(-\d+),\"(([0-9a-f]{2})(:([0-9a-f]{2})){5})\",(\d+),(-*\d+),(\d+)\)"
    match = re.match(pattern, CWLAPLine)
    securityStr = match.group(1)
    self.security = int(securityStr)
    self.ssid = match.group(2)
    rssiStr = match.group(3)
    self.rssi = int(rssiStr)
    self.bssid = match.group(4)
    channelStr = match.group(8)
    self.channel = int(channelStr)
    freqOffsetStr = match.group(9)
    self.freqOffset = int(freqOffsetStr)
    freqCalStr = match.group(10)
    self.freqCal = int(freqCalStr)

  def _setup3(self, CWSAPLine, default):
    pattern = '\+CWSAP_'
    if default:
      pattern += ESP8266ATCommandMode.DEF
    else:
      pattern += ESP8266ATCommandMode.CUR
    pattern += ':\"(.+?)\",\"(.+?)\",(\d+),(\d+),(\d+),(\d+)'
    match = re.match(pattern, CWSAPLine)
    self.ssid = match.group(1)
    self.pwd = match.group(2)
    channelStr = match.group(3)
    self.channel = int(channelStr)
    securityStr = match.group(4)
    self.security = int(securityStr)
    maxConnStr = match.group(5)
    self.maxConn = int(maxConnStr)
    hiddenStr = match.group(6)
    hiddenInt = int(hiddenStr)
    if hiddenInt == 0:
      self.hidden = False
    elif hiddenInt == 1:
      self.hidden = True
    else:
      raise ESP8266ATCommandError()

  def getSecurity(self):
    return self.security

  def getSSID(self):
    return self.ssid

  def getPassword(self):
    return self.pwd

  def getRSSI(self):
    return self.rssid

  def getBSSID(self):
    return self.bssid

  def getChannel(self):
    return self.channel

  def getQFrequencyOffset(self):
    return self.freqOffset

  def getFrequencyCalibration(self):
    return self.freqCal

  def getMaxConnections(self):
    return self.maxConn

  def isHidden(self):
    return self.hidden

  def __str__(self):
    s = "SSID:\t\t%s" % self.ssid
    if self.security is not None:
      if(self.security == ESP8266ATCommandMode.WIFI_SECURITY_OPEN):
        security = "Open"
      elif(self.security == ESP8266ATCommandMode.WIFI_SECURITY_WEP):
        security = "WEP"
      elif(self.security == ESP8266ATCommandMode.WIFI_SECURITY_WPA_PSK):
        security = "WPA-PSK"
      elif(self.security == ESP8266ATCommandMode.WIFI_SECURITY_WPA2_PSK):
        security = "WPA2-PSK"
      elif(self.security == ESP8266ATCommandMode.WIFI_SECURITY_WPA_WPA2_PSK):
        security = "WPA-PSK / WPA2-PSK"
      else:
        security = "Unknown"
      s += '\r\nSecurity:\t%s' % security
    if self.pwd is not None:
      s += '\r\nPassword:\t%s' % self.pwd
    if self.rssi is not None:
      s += '\r\nRSSI:\t\t%i dBm' % self.rssi
    if self.bssid is not None:
      s += '\r\nBSSID:\t\t%s' % self.bssid
    if self.channel is not None:
      s += '\r\nChannel:\t%i' % self.channel
    if self.freqOffset is not None:
      s += '\r\nFreq. off:\t%i kHz' % self.freqOffset
    if self.freqCal is not None:
      s += '\r\nFreq. cal:\t%i' % self.freqCal
    if self.maxConn is not None:
      s += '\r\nMax conn.:\t%i' % self.maxConn
    if self.hidden is not None:
      s += '\r\nHidden:\t\t'
      if self.hidden:
        s += "yes"
      else:
        s += "no"
    return s


class ESP8266ATCommandMode:

  DEFAULT_PORT = "COM3"
  DEFAULT_BAUDRATE = 115200
  DEFAULT_TIMEOUT = 2
  ERROR_MSG = 'ERROR'
  STATUS_MSG = ['WIFI CONNECTED', 'WIFI GOT IP', 'WIFI DISCONNECT']
  STATUS_LOG_TIME_FORMAT = "%Y-%M-%d %H:%M:%S.%f"

  DEFAULT_WIFI_MODE_FROM = 1
  DEFAULT_WIFI_MODE_TO = 3
  DEFAULT_BUSY_WAIT = 3

  DEF = 'DEF'
  CUR = 'CUR'
  MSG_OK = 'OK'
  MSG_FAIL = 'FAIL'

  WIFI_MODE_STA = 1
  WIFI_MODE_AP = 2
  WIFI_MODE_STA_AP = 3

  WIFI_SECURITY_OPEN = 0
  WIFI_SECURITY_WEP = 1
  WIFI_SECURITY_WPA_PSK = 2
  WIFI_SECURITY_WPA2_PSK = 3
  WIFI_SECURITY_WPA_WPA2_PSK = 4

  WIFI_AP_SCAN_SHOW_SECURITY = 1
  WIFI_AP_SCAN_SHOW_SSID = 2
  WIFI_AP_SCAN_SHOW_RSSI = 4
  WIFI_AP_SCAN_SHOW_BSSID = 8
  WIFI_AP_SCAN_SHOW_CHANNEL = 16
  WIFI_AP_SCAN_SHOW_FREQ_OFFSET= 32
  WIFI_AP_SCAN_SHOW_FREQ_CAL = 64

  WIFI_AP_JOIN_CONNECTED = 0
  WIFI_AP_JOIN_FAILED_TIMEOUT = 1
  WIFI_AP_JOIN_FAILED_WRONG_PASS = 2
  WIFI_AP_JOIN_FAILED_SSID_NOT_FOUND = 3
  WIFI_AP_JOIN_FAILED_OTHER = 4

  DHCP_AP = 0
  DHCP_STA = 1
  DHCP_STA_AP = 2


  def __init__(self, port=None, baudrate=None, timeout=None, busyWait=None):
    if port is None:
      port = self.DEFAULT_PORT
    if baudrate is None:
      baudrate = self.DEFAULT_BAUDRATE
    if timeout is None:
      timeout = self.DEFAULT_TIMEOUT
    if busyWait is None:
      self.busyWait = self.DEFAULT_BUSY_WAIT
    else:
      self.busyWait = busyWait
    self.ser = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)
    self.WiFiModeFrom = self.DEFAULT_WIFI_MODE_FROM
    self.WiFiModeTo = self.DEFAULT_WIFI_MODE_TO
    self.statusMsgLog = list()

  def __write(self, cmd, noecho=False):
    data = cmd + "\r\n"
    data = bytes(data, 'utf-8')
    self.ser.write(data)

  def __read1(self, line):

    if isinstance(line, str):
      raise ESP8266TimeoutError
    line = line[:-2]
    line = line.decode('utf-8')
    return line

  def __read(self):
    line = self.ser.readline()
    line = self.__read1(line)
    if line == "busy p...":
      time.sleep(self.busyWait)
      return self.__read()
    elif line == "":
      return self.__read()
    elif line in self.STATUS_MSG:
      self.__addStatusMsg(line)
      return self.__read()
    elif line == self.ERROR_MSG:
      raise ESP8266ATCommandError("received %s" % self.ERROR_MSG)
    return line

  def __readBytes(self):
    line = self.ser.readline()
    return line

  def __readCmdEcho(self, cmd):
    line = self.__read()
    try:
      assert line[-1] == "\r"
    except AssertionError:
      e = "Command not echoed properly - expected CR at end of '%s'" % line
      raise ESP8266ATCommandError(e)
    line = line[:-1]
    try:
      assert line == cmd
    except AssertionError:
      e = "Command not echoed properly - expected '%s', got '%s'" % (cmd, line)
      raise ESP8266ATCommandError(e)

  def readStatusMsgs(self):
    lines = self.ser.readlines()
    for line in lines:
      line = self.__read1(line)
      if line in self.STATUS_MSG:
        self.__addStatusMsg(line)

  def __addStatusMsg(self, msg):
    now = datetime.now()
    self.statusMsgLog.append((now, msg))

  def getStatusMsgLog(self):
    return self.statusMsgLog

  def printStatusMsgLog(self):
    for time, msg in self.statusMsgLog:
      print("[%s] %s" % (time.strftime(self.STATUS_LOG_TIME_FORMAT), msg))

  def attention(self):
    cmd = 'AT'
    try:
      self.__write(cmd)
    except ESP8266TimeoutError:
      return False
    self.__readCmdEcho(cmd)
    line = self.__read()
    try:
      assert line == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()
    return True

  def reset(self):
    cmd = 'AT+RST'
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    try:
      assert line == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()
    output = b''
    while(True):
      line = self.__readBytes()
      output += line
      if line == b'ready\r\n':
        break
    return str(output)

  def getFirmwareVersion(self):
    cmd = 'AT+GMR'
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    output = ""
    while(True):
      line = self.__read()
      if line == self.MSG_OK:
        break
      output += line + "\r\n"
    firmware = ESP8266Firmware(output)
    return firmware





  def getWiFiMode(self, default=False):
    cmd = 'AT+CWMODE_'
    pattern = '\+CWMODE_'
    if default:
      cmd += self.DEF
      pattern += self.DEF
    else:
      cmd += self.CUR
      pattern += self.CUR
    cmd += '?'
    pattern += ':(\d)'
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    line1 = self.__read()
    try:
      assert line1 == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()
    mode = re.match(pattern, line).group(1)
    mode = int(mode)
    return mode

  def queryWiFiModeScope(self, default=False):
    cmd = 'AT+CWMODE_'
    pattern = '\+CWMODE_'
    if default:
      cmd += self.DEF
      pattern += self.DEF
    else:
      cmd += self.CUR
      pattern += self.CUR
    cmd += '=?'
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    pattern += ':\((\d+)-(\d+)\)'
    match = re.match(pattern, line)
    try:
      assert match is not None
    except AssertionError:
      raise ESP8266ATCommandError()
    line = self.__read()
    try:
      assert line == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()
    modeFrom = match.group(1)
    modeTo = match.group(2)
    modeFrom = int(modeFrom)
    modeTo = int(modeTo)
    try:
      assert modeFrom <= modeTo
    except AssertionError:
      e = "invalid Wi-Fi mode range - (%i - %i)" % (modeFrom, modeTo)
      raise ESP8266ATCommandError(e)
    self.WiFiModeFrom = modeFrom
    self.WiFiModeTo = modeTo
    return (self.WiFiModeFrom, self.WiFiModeTo)

  def __setWiFiMode1(self, mode):
    if not isinstance(mode, int):
      e = "request to set invalid Wi-Fi mode - '%s'" % str(mode)
      raise ESP8266ATCommandError(e)

  def __setWiFiMode2(self, mode, default):
    cmd = 'AT+CWMODE_'
    if default:
      cmd += self.DEF
    else:
      cmd += self.CUR
    cmd += "=%s" % str(mode)
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    try:
      assert line == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()

  def setWiFiMode(self, mode, default=False):
    self.__setWiFiMode1(mode)
    self.queryWiFiModeScope(default=default)
    try:
      assert mode >= self.WiFiModeFrom and mode <= self.WiFiModeTo
    except AssertionError:
      e = "request to set out of range Wi-Fi mode: %i (range: %i - %i)" % (mode, self.WiFiModeFrom, self.WiFiModeTo)
      raise ESP8266ATCommandError(e)
    self.__setWiFiMode2(mode, default)

  def setWiFiModeRaw(self, mode, default=False):
    self.__setWiFiMode1(mode)
    self.__setWiFiMode2(mode, default)





  def joinWiFiAP(self, ssid, pwd, bssid=None, default=False):
    cmd = 'AT+CWJAP_'
    if default:
      cmd += self.DEF
    else:
      cmd += self.CUR
    cmd += '="%s","%s"' % (ssid, pwd)
    if bssid is not None:
      cmd += ',"%s"' % bssid
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    if line == self.MSG_OK:
      return self.WIFI_AP_JOIN_CONNECTED
    else:
      pattern = '\+CWJAP:(\d)'
      match = re.match(pattern, line)
      try:
        assert match is not None
      except AssertionError:
        raise ESP8266ATCommandError()
      line = self.__read()
      try:
        assert line == self.MSG_FAIL
      except AssertionError:
        raise ESP8266ATCommandError()
      errorCode = match.group(1)
      errorCode = int(errorCode)
      return errorCode

  def getConnectedWiFiAP(self, default=False):
    cmd = 'AT+CWJAP_'
    if default:
      cmd += self.DEF
    else:
      cmd += self.CUR
    cmd += '?'
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    if(line == "No AP"):
      return -1
    else:
      line1 = self.__read()
      try:
        assert line1 == self.MSG_OK
      except AssertionError:
        raise ESP8266ATCommandError()
      AP = ESP8266AccessPoint()
      AP._setup1(line, default)
      return AP





  def setScanWiFiAPConfig(self, orderByRSSI=False, showParameters=127):
    cmd = 'AT+CWLAPOPT=%i,%i' % (orderByRSSI, showParameters)
    self.__write(cmd, noecho=True)
    line = self.__read()
    try:
      assert line == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()
    self.__readCmdEcho(cmd)

  def scanWiFiAPs(self, ssid=None, bssid=None, channel=None):
    cmd = 'AT+CWLAP'
    ssidSet = False
    bssidSet = False
    if ssid is not None:
      ssidSet = True
      cmd += '="%s"' % ssid
    if bssid is not None:
      try:
        assert ssidSet
      except AssertionError:
        raise ESP8266ATCommandError("SSID has to be set with BSSID")
      bssidSet = True
      cmd += ',"%s"' % bssid
    if channel is not None:
      try:
        assert ssidSet
        assert bssidSet
      except AssertionError:
        raise ESP8266ATCommandError("SSID and BSSID have to be set with channel")
      cmd += ',%i' % channel
    self.__write(cmd)
    print(cmd)
    self.__readCmdEcho(cmd)
    APs = list()
    while(True):
      line = self.__read()
      # print("line: %s" % line)
      if line == self.MSG_OK:
        break
      AP = ESP8266AccessPoint()
      AP._setup2(line)
      APs.append(AP)
    return APs




  def disconnectWiFiAP(self):
    cmd = "AT+CWQAP"
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    try:
      assert line == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()






  def getSoftAPConfig(self, default=False):
    cmd = 'AT+CWSAP_'
    if default:
      cmd += self.DEF
    else:
      cmd += self.CUR
    cmd += '?'
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    line1 = self.__read()
    try:
      assert line1 == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()
    AP = ESP8266AccessPoint()
    AP._setup3(line, default)
    return AP

  def setSoftAPConfig(self, ssid, pwd, channel, security, maxConn=None, ssidHidden=None, default=False):
    cmd = 'AT+CWSAP_'
    if default:
      cmd += self.DEF
    else:
      cmd += self.CUR
    cmd += '="%s","%s",%i,%i' % (ssid, pwd, channel, security)
    maxConnSet = False
    if maxConn is not None:
      maxConnSet = True
      cmd += ',%i' % maxConn
    if ssidHidden is not None:
      try:
        assert maxConn is not None
      except AssertionError:
        raise ESP8266ATCommandError("Max conn. has to be set with SSID hidden")
      cmd += ',%i' % ssidHidden
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    try:
      assert line == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()





  def getConnectedStations(self):
    cmd = 'AT+CWLIF'
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    stations = list()
    pattern = '(.+),(.+)'
    while(True):
      line = self.__read()
      if line == self.MSG_OK:
        break
      match = re.match(pattern, line)
      IPAddr = match.group(1)
      MACAddr = match.group(2)
      stations.append((IPAddr, MACAddr))
    return stations





  def getDHCPState(self, default=False):
    cmd = 'AT+CWDHCP_'
    pattern = '\+CWDHCP_'
    if default:
      cmd += self.DEF
      pattern += self.DEF
    else:
      cmd += self.CUR
      pattern += self.CUR
    cmd += '?'
    pattern += ':(\d)'
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    line1 = self.__read()
    try:
      assert line1 == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()
    match = re.match(pattern, line)
    dhcp = match.group(1)
    dhcp = int(dhcp)
    softAPDHCP = dhcp & 1
    stationDHCP = dhcp & 2
    softAPDHCP = bool(softAPDHCP)
    stationDHCP = bool(stationDHCP)
    return softAPDHCP, stationDHCP

  def setDHCPState(self, mode, state, default=False):
    cmd = 'AT+CWDHCP_'
    if default:
      cmd += self.DEF
    else:
      cmd += self.CUR
    cmd += '=%i,%i' % (mode, state)
    self.__write(cmd)
    self.__readCmdEcho(cmd)
    line = self.__read()
    try:
      assert line == self.MSG_OK
    except AssertionError:
      raise ESP8266ATCommandError()